#!/usr/bin/env python3
# coding: utf-8

"""Mailfud GeoIP legacy databases conversion script
(c) The Doomseeker Team 2016 - 2018, 2022.

This script converts Mailfud's database from its original CSV format into
compacted format known to Doomseeker. Compacted format is optimized for faster
loading and for searching country by IP. The specs are described in the
IpToCountry_*.dat.SPECS.txt files.

Database can either be already stored in a locally accessible filesystem or the
script can automatically download the newest version directly from Mailfud's
website. Run with '-h' argument for usage details.

----------------------------------------

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301  USA

----------------------------------------

Mailfud GeoIP legacy databases
https://mailfud.org/geoip-legacy/

Mailfud GeoIP legacy is licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License. To view a copy of this
license, visit http://creativecommons.org/licenses/by-sa/4.0/.

"""

import argparse
import csv
import json
import gzip
import hashlib
import os
import shutil
import struct
import sys
import tempfile
from collections import defaultdict, OrderedDict
from multiprocessing import Pool
from urllib.request import urlopen

import pycountry

country_mapping = None


class Licence:
    def __init__(self, locale, licence):
        self._locale = locale
        self._licence = licence

    @property
    def locale(self):
        return self._locale

    @property
    def licence(self):
        return self._licence


licences = [
    Licence(
        "",
        ("Mailfud GeoIP legacy databases are provided with "
         "Creative Commons Attribution-ShareAlike 4.0 International "
         "License.")),
    Licence(
        "ca_ES",
        ('La base de dades "Mailfud GeoIP legacy" es proporciona sota la '
         '"Creative Commons Attribution-ShareAlike 4.0 International License".')),
    Licence(
        "es_ES",
        ('La base de datos "Mailfud GeoIP legacy" se proporciona bajo la '
         '"Creative Commons Attribution-ShareAlike 4.0 International License".')),
    Licence(
        "pl_PL",
        ("Bazy danych Mailfud GeoIP legacy są rozpowszechniane "
         "na licencji Creative Commons Attribution-ShareAlike 4.0 "
         "International License.")),
]

licence_url = "https://mailfud.org/geoip-legacy/"

def licence_by_locale(locale):
    for licence in licences:
        if licence.locale == locale:
            return licence
    raise KeyError(locale)


url = "https://mailfud.org/geoip-legacy/GeoIP-legacy.csv.gz"

translate_alpha2_to_alpha3 = {}
for country in pycountry.countries:
    translate_alpha2_to_alpha3[country.alpha_2] = country.alpha_3
translate_alpha2_to_alpha3["EU"] = "EU"


# Returns the integer representation of the ip, or 0 if it is not an IP
def validate_ip_to_int(s):
    a = s.split('.')
    if len(a) != 4:
        return 0
    ip_as_int = 0
    for x in a:
        if not x.isdigit():
            return 0
        i = int(x)
        if i < 0 or i > 255:
            return 0
        ip_as_int = (ip_as_int << 8) + i
    return ip_as_int


def download_db():
    url = "https://mailfud.org/geoip-legacy/GeoIP-legacy.csv.gz"
    f = None
    try:
        with tempfile.NamedTemporaryFile(delete=False) as f:
            download_url(url, f)
        tmpfile = tempfile.mktemp()
        ungzip(f.name, tmpfile)
    finally:
        if f:
            try:
                os.unlink(f.name)
            except Exception as e:
                _prerr("Warning: failed to delete file {0}:\n\t{1}".format(f.name, e))
    return tmpfile


def download_url(url, outfile):
    stream = urlopen(url)
    try:
        _prerr("Receiving URL {0}".format(url))
        collected = 0
        while True:
            chunk = stream.read(1024 * 20)
            if not chunk:
                break
            collected += len(chunk)
            _prerr("Downloaded {0} B".format(collected))
            outfile.write(chunk)
    finally:
        stream.close()


def ungzip(zippath, outfile):
    _prerr("Extracting downloaded file '{0}'\n\tto file '{1}'".format(
        zippath, outfile))

    with gzip.open(zippath, 'rb') as f_in:
        with open(outfile, 'wb+') as f_out:
            shutil.copyfileobj(f_in, f_out)


def reduce_block(block):
    geoid, ipranges = block
    _prerr("geoid {0}, networks {1}\n".format(geoid, len(ipranges)))
    old_size = len(ipranges)
    ipranges = merge_consecutive_ipranges(ipranges)
    _prerr("geoid {0} - DONE, networks reduced from {1} to {2}\n".format(
        geoid, old_size, len(ipranges)))
    return geoid, ipranges


# Flattens consecutive and overlapping ipranges
def merge_consecutive_ipranges(ipranges):
    # By sorting by the start address we will know that as soon as we find a
    # non-consecutive range we have finished merging the previous range.
    ipranges = sorted(ipranges, key=lambda el: el[0])

    merged = [ipranges[0]]
    for iprange in ipranges[1:]:
        # Check if this next range is consecutive or overlapping the last range.
        # Given the sort we already know that iprange[0] >= merged_range[0]
        merged_range = merged[-1]
        if iprange[0] - merged_range[1] <= 1:
            merged[-1] = (merged_range[0], max(merged_range[1], iprange[1]))
            continue

        merged.append(iprange)
    return merged


def compute_ip_data(geolite_blocks_path):
    with open(geolite_blocks_path, "r") as f:
        reader = csv.reader(f)
        countries = {}
        ips = defaultdict(list)
        for row in reader:
            ip_range = (validate_ip_to_int(row[0]), validate_ip_to_int(row[1]))
            if ip_range[0] == 0 or ip_range[1] == 0:
                continue
            geo_id = row[4]
            geo_name = row[5]
            if not geo_id:
                continue
            countries[geo_id] = geo_name
            ips[geo_id].append(ip_range)

    pool = Pool()
    try:
        items = list(ips.items())
        items.sort(key=lambda el: len(el[1]), reverse=True)
        blocks = dict(pool.imap_unordered(reduce_block, items))
    finally:
        pool.terminate()
        pool.close()

    return countries, blocks


def generate_ip_data(locations, blocks):
    data = []
    for geoid, location in locations.items():
        data.append(translate_alpha2_to_alpha3.get(geoid, "USA").encode("utf-8") + b"\0")
        data.append(struct.pack(b"<I", len(blocks[geoid])))
        for network in blocks[geoid]:
            data.append(struct.pack(b"<II", network[0], network[1]))
    return data


def generate_licence_data():
    data = []
    for licence in licences:
        data.append(licence.locale.encode("utf-8") + b"\0")
        data.append(licence.licence.encode("utf-8") + b"\0")
    return data


def generate_url_data():
    return [url.encode("utf-8") + b"\0"]


def convert_v2(ip_data, output_path):
    countries, blocks = ip_data

    with open(output_path, "wb") as f:
        f.write(b"IP2C")
        f.write(struct.pack(b"<H", 2))  # Version
        for geoid, country_blocks in blocks.items():
            country_code = translate_alpha2_to_alpha3.get(geoid, "USA")
            country_name = countries[geoid]
            f.write(country_name.encode("utf-8") + b"\0")
            f.write(country_code.encode("utf-8") + b"\0")
            f.write(struct.pack(b"<I", len(country_blocks)))
            for iprange in country_blocks:
                f.write(struct.pack(b"<II", iprange[0], iprange[1]))


def convert_v3(ip_data, output_path):
    countries, blocks = ip_data

    with open(output_path, "wb") as f:
        f.write(b"IP2C")
        f.write(struct.pack(b"<H", 3))  # Version
        datas = OrderedDict([
            (0, generate_licence_data()),
            (1, generate_ip_data(countries, blocks)),
            (2, generate_url_data())
        ])

        for key, value in datas.items():
            f.write(struct.pack(b"<B", key))  # ID section
            total_size = 0
            for entry in value:
                total_size += len(entry)
            f.write(struct.pack(b"<Q", total_size))
            for entry in value:
                f.write(entry)


def md5(fpath):
    with open(fpath, "rb") as in_file:
        checksum = hashlib.md5(in_file.read()).hexdigest()
        _prerr("MD5: {0} - {1}".format(checksum, fpath))
        # Create file that is supposed to be deployed
        # on our server in the IP2C update provider.
        with open("{}.md5".format(fpath), "w") as md5_file:
            md5_file.write(checksum)
            md5_file.write("\n")


def gzip_file(fpath):
    # gzip filename is hardcoded because that's the name under
    # which we serve it from our home page.
    gzpath = "{}.gz".format(fpath)
    with gzip.open(gzpath, "wb") as gfile:
        with open(fpath, "rb") as in_file:
            gfile.write(in_file.read())
    _prerr("gzip file generated at: {0}".format(gzpath))


def licence_php(php_path, licence, url):
    # The file goes into the "ip2c" subdirectory on the website.
    template = """
<?php
define('URL_PREFIX', '../');
include '../header.php';
?>
<h1>IP2C Licence</h1>
<p>{licence}</p>
<p><a href="{url}">{url}</a></p>
<?php
include '../footer.php';
?>
"""
    with open(php_path, "w") as f_out:
        f_out.write(template.format(licence=licence.licence, url=url))


def run():
    argp = argparse.ArgumentParser()
    argp.add_argument("-s", "--save", help="save the downloaded database; forces download")
    argp.add_argument("--v2", help="output filename for IpToCountry.dat version 2")
    argp.add_argument("--v3", help="output filename for IpToCountry.dat version 3")
    argp.add_argument("geoip", nargs="?", help=(
        "the source GeoIP-legacy.csv file; "
        "if unspecified, will be auto-downloaded"))
    args = argp.parse_args()
    if not any([args.save, args.v2, args.v3]):
        argp.error("must specify at least: -s, --v2 or --v3")

    scriptdir = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(scriptdir, "country_iso3.json"), "r") as f:
        global country_mapping
        country_mapping = json.loads(f.read())

    downloaded_db = None
    try:
        if not args.geoip or args.save:
            downloaded_db = download_db()
            if not args.geoip:
                args.geoip = downloaded_db
            if args.save:
                shutil.copy(downloaded_db, args.save)

        if any([args.v2, args.v3]):
            ip_data = compute_ip_data(args.geoip)
            if args.v2:
                convert_v2(ip_data, args.v2)
            if args.v3:
                convert_v3(ip_data, args.v3)
    finally:
        if downloaded_db:
            try:
                os.unlink(downloaded_db)
            except Exception as e:
                _prerr("Warning: failed to clean up tmpfile '{0}': {1}".format(downloaded_db, e))
    for fpath in [args.v2, args.v3]:
        if fpath:
            md5(fpath)
            gzip_file(fpath)
    if args.v2:
        licence_php("{}.php".format(args.v2), licence=licence_by_locale(""),
                    url=licence_url)


def _prerr(*args, **kwargs):
    kwargs.setdefault("file", sys.stderr)
    print(*args, **kwargs)


if __name__ == "__main__":
    run()
