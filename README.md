# doomseeker-blobs

Storage repository for binary blob data used by Doomseeker such as IP2C

# Mailfud GeoIP legacy databases

This product includes GeoIP data made available by Mailfud,
available from https://mailfud.org/geoip-legacy/.

Mailfud GeoIP legacy databases are licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License. To view a copy of this
license, visit http://creativecommons.org/licenses/by-sa/4.0/.

----

# This File

Copyright (C) 2018, 2022 The Doomseeker Team

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
